# PlantScopeFilter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **int** |  | [optional] 
**limit** | **int** |  | [optional] 
**skip** | **int** |  | [optional] 
**order** | [**OneOfPlantScopeFilterOrder**](OneOfPlantScopeFilterOrder.md) |  | [optional] 
**where** | **dict(str, object)** |  | [optional] 
**fields** | [**OneOfPlantScopeFilterFields**](OneOfPlantScopeFilterFields.md) |  | [optional] 
**include** | **list[dict(str, object)]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

