## Generating this Library

This library should be updated whenever a change is released
to the production Sundial API.

This should be automated in future.

For now, however, do something like this:

```
 git clone git@github.com:swagger-api/swagger-codegen.git
 cd swagger-codegen/
 git checkout 3.0.0
 ./run-in-docker.sh mvn package
 ./run-in-docker.sh generate -i https://api.sundial.energy/openapi.json -l python -o /gen/out/python-sundial -DpackageName=sundial_energy
```

And copy the contents of `out/python-sundial` to this code directory.
