# sundial_energy.MeasurementControllerApi

All URIs are relative to *https://api.dev.sundial.energy*

Method | HTTP request | Description
------------- | ------------- | -------------
[**measurement_controller_create**](MeasurementControllerApi.md#measurement_controller_create) | **POST** /plants/{id}/measurements | 

# **measurement_controller_create**
> Measurement measurement_controller_create(id, body=body)



### Example
```python
from __future__ import print_function
import time
import sundial_energy
from sundial_energy.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = sundial_energy.MeasurementControllerApi(sundial_energy.ApiClient(configuration))
id = 1.2 # float | 
body = sundial_energy.NewMeasurementInPlant() # NewMeasurementInPlant |  (optional)

try:
    api_response = api_instance.measurement_controller_create(id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MeasurementControllerApi->measurement_controller_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **float**|  | 
 **body** | [**NewMeasurementInPlant**](NewMeasurementInPlant.md)|  | [optional] 

### Return type

[**Measurement**](Measurement.md)

### Authorization

[jwt](../README.md#jwt)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

