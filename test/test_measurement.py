# coding: utf-8

"""
    sundial

    Sundial optimises renewable power generation  # noqa: E501

    OpenAPI spec version: 1.2.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import sundial_energy
from sundial_energy.models.measurement import Measurement  # noqa: E501
from sundial_energy.rest import ApiException


class TestMeasurement(unittest.TestCase):
    """Measurement unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testMeasurement(self):
        """Test Measurement"""
        # FIXME: construct object with mandatory attributes with example values
        # model = sundial_energy.models.measurement.Measurement()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
